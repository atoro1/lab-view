from django.contrib import admin
from .models import *


@admin.register(Lab)
class LabAdmin(admin.ModelAdmin):
    list_display = ['name', 'nickname']


@admin.register(Rack)
class RackAdmin(admin.ModelAdmin):
    list_display = ['name', 'nickname']


@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    list_display = ['name', 'nickname']
