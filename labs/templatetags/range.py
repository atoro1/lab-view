from django import template
from django.db.models import Max

register = template.Library()


@register.filter
def getrange(num=1):
    return range(num + 1)


@register.filter
def row(obj, obj_row):
    return obj.filter(order=obj_row)


@register.filter
def get_max(obj, col):
    return obj.aggregate(Max(col))[f'{col}__max']
