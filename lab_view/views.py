from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views import generic
from labs.models import Lab


class HomeView(generic.TemplateView):
    """
    Home view with basic information
    """
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['labs'] = Lab.objects.order_by('-name')
        return context


class RegisterView(SuccessMessageMixin, generic.CreateView):
    """
    Registration view for new users
    """
    template_name = 'registration/register.html'
    model = User
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    success_message = '%(username)s was created successfully. Please log in.'
