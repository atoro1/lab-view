# Lab View

## Purpose
To create an easily browsable visualization of a server lab environment.
With full-size racks showing what hardware is located where,
and the ability to create multiple labs, each with their own layout
and racks with their own systems.

This was purpose-built as an internal tool for a niche use-case, but
can easily be expanded or modified to fit any server farm needs.

## Setup
For quick set, follow the steps below.

1. `git pull` this repo to your system
2. Make sure you have the latest Python installed from python.org (3.9 as of writing)
3. Create a virtual environment for Python in the project directory
   1. Windows: `py -m venv venv`
   2. Linux: `python3.9 -m venv venv`
4. Activate the venv and install the required packages
   1. Windows: `.\venv\Scripts\activate` > `pip install -r requirements.txt`
   2. Linux: `source venv\bin\activate` > `pip install -r requirements.txt`
5. Set up the local database 
   1. `python manage.py makemigrations`
   2. `python manage.py migrate`
6. Create your first superuser
   1. `python manage.py createsuperuser`
   2. Follow on-screen instructions
7. Run the project locally
   1. `python manage.py runproject`
8. Navigate to http://127.0.0.1:8000 and enjoy
9. Add/edit information on the admin panel after logging in