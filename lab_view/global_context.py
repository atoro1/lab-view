from labs.models import Lab


def information(request):
    context = {}
    if request.user.is_authenticated:
        user_labs = Lab.objects.filter(authorized__in=[request.user]).order_by('-name')
        context = {'user_labs': user_labs}
    return context
