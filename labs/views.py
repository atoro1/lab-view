from django.views import generic
from .models import Lab, Rack, Server


class LabView(generic.DetailView):
    model = Lab

    template_name = 'labs/lab.html'


class RackView(generic.DetailView):
    model = Rack

    template_name = 'labs/rack.html'
