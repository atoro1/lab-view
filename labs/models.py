from django.db import models
from django.contrib.auth.models import User


class Lab(models.Model):
    """
    Representation of a lab environment.
    May contain any number of racks, which
    may contain any number of servers
    """
    name = models.CharField(max_length=200)
    nickname = models.CharField(max_length=20)
    rows = models.SmallIntegerField(default=1)
    authorized = models.ManyToManyField(User)

    def __str__(self):
        return self.name


class Rack(models.Model):
    """
    Representation of a server rack.
    May contain any number of servers. KVM is
    optional, link will not show up if null.
    Connected to Lab via foreign key.
    """
    name = models.CharField(max_length=200)
    nickname = models.CharField(max_length=20)
    lab = models.ForeignKey(Lab, on_delete=models.CASCADE)
    row = models.SmallIntegerField(default=1)
    order = models.SmallIntegerField(default=1)
    kvm = models.URLField(null=True, blank=True)
    color = models.CharField(max_length=30, blank=True, null=True)
    dark = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Server(models.Model):
    """
    Representation of a server.
    Connected to Rack via foreign key.
    """
    name = models.CharField(max_length=200)
    nickname = models.CharField(max_length=20)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE)
    order = models.SmallIntegerField(default=1)
    size = models.SmallIntegerField(default=1)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-order']
