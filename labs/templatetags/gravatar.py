import hashlib
from django import template

register = template.Library()


@register.filter
def gravatar(email, size=40):
    return f'https://www.gravatar.com/avatar/{hashlib.md5(email.lower()).hexdigest()}?s={size}&d=mp'
