from django.urls import path
from .views import LabView, RackView

urlpatterns = [
    path('<int:pk>/', LabView.as_view(), name='lab'),
    path('rack/<int:pk>/', RackView.as_view(), name='rack')
]
